---
name: "PinePhone Pro"
deviceType: "phone"
buyLink: "https://pine64.com/"
price:
  avg: 400
  currency: "USD"
subforum: "88/pinephone"
description: "The PinePhone Pro is PINE64's flagship smartphone. It features 4GB of RAM, an 128GB eMMC and is using an Rockchip RK3399S SoC, which is a specialized version of the RK3399 made specifically for the PinePhone Pro."

deviceInfo:
  - id: "cpu"
    value: "2x 1.5GHz Cortex-A72 + 4x 1.5GHz Cortex-A53"
  - id: "chipset"
    value: "Rockchip RK3399S"
  - id: "gpu"
    value: "Mali-T860 500MHz"
  - id: "rom"
    value: "128 GB"
  - id: "ram"
    value: "4 GB LPDDR4 800MHz"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: '5.99" 720x1440 IPS'
  - id: "rearCamera"
    value: "Single 13MP, 1/3″, LED Flash"
  - id: "frontCamera"
    value: "Single 8MP, 1/4″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "160.8mm x 76.6mm x 11.1mm"
  - id: "weight"
    value: "215 grams"
  - id: "releaseDate"
    value: "15.10.2021"

sources:
  portType: "external"
  issuesLink: "https://gitlab.com/ook37/pinephone-pro-debos/-/issues"

externalLinks:
  - name: "Sources"
    link: "https://gitlab.com/ook37/pinephone-pro-debos/"
  - name: "CI Builds"
    link: "https://gitlab.com/ook37/pinephone-pro-debos/-/releases"
    noActivityDetection: true

communityHelp:
  - name: "Ubuntu Touch subforum on Pine64 Forum"
    link: "https://forum.pine64.org/forumdisplay.php?fid=125"
  - name: "Pinephone subreddit"
    link: "https://www.reddit.com/r/pinephone/"
  - name: "IRC ( #pinephone on irc.pine64.org )"
    link: "https://www.pine64.org/web-irc/"
  - name: "#pinephone on Pine64 Discord"
    link: "https://discord.com/invite/DgB7kzr"
  - name: "#pinephone:matrix.org"
    link: "https://matrix.to/#/#pinephone:matrix.org"
  - name: "Telegram chat"
    link: "https://t.me/utonpine"

contributors:
  - name: "Pine64"
    role: "Phone maker"
  - name: "Oren Klopfer"
    role: "Developer"
---
