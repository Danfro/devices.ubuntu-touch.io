---
name: "Planet Computers Cosmo Communicator"
deviceType: "phone"
buyLink: "https://store.planetcom.co.uk/products/cosmo-communicator"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53, ARM Cortex-A73 (4x 2.0 GHz + 4x 2.1 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P70, MT6771T"
  - id: "gpu"
    value: "ARM Mali-G72 MP3 @ 900 MHz, 3 cores"
  - id: "rom"
    value: "128 GB, eMMC"
  - id: "ram"
    value: "6 GB, DDR3"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4200 mAh, Li-Polymer"
  - id: "display"
    value: '5.99" IPS, 1080 x 2160 (403 PPI), Rounded corners'
  - id: "rearCamera"
    value: "24MP, LED flash"
  - id: "frontCamera"
    value: "5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "171 mm x 79.3 mm x 17.3 mm"
  - id: "weight"
    value: "326 g"
contributors:
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
  - name: Davide Guidi
    forum: https://github.com/dguidipc
sources:
  portType: "external"
  issuesLink: "https://gitlab.com/ubports/porting/community-ports/android9/planet-cosmocom/planet-cosmocom/-/issues"
externalLinks:
  - name: "Device source"
    link: "https://gitlab.com/ubports/community-ports/android9/planet-cosmocom"
    noActivityDetection: true
  - name: "Kernel source"
    link: "https://github.com/gemian/cosmo-linux-kernel-4.4/tree/ubports"
    noActivityDetection: true
  - name: "Installation instructions"
    link: "https://github.com/gemian/gemian/wiki/UBPorts"
    noActivityDetection: true
---
