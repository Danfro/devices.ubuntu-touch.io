---
name: "Xiaomi Redmi 3X, 3S and 3SP"
comment: "wip"
deviceType: "phone"
image: "http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-3-pro-2.jpg"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 1.3 GHz@4 1,0ghz@4 Cortex-A53"
  - id: "chipset"
    value: "Qualcomm MSM8937 Snapdragon 430"
  - id: "gpu"
    value: "Adreno 505"
  - id: "rom"
    value: "16 GB / 32 GB"
  - id: "ram"
    value: "2 GB / 3 GB"
  - id: "display"
    value: "720x1280"
  - id: "rearCamera"
    value: "13 MP, LED flash"
  - id: "frontCamera"
    value: "5 MP, No flash"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "139.3 x 69.6 x 8.5 mm"

sources:
  portType: "external"

externalLinks:
  - name: "Source repos"
    link: "https://github.com/Ubports-Land"
    noActivityDetection: true
  - name: "CI builds"
    link: "https://github.com/Ubports-Land/Ubports-CI"

communityHelp:
  - name: "Telegram group - @utland"
    link: "https://t.me/utland"

contributors:
  - name: "DarknessHidden"
    forum: "t.me/DarknessHiddenorg"
---
