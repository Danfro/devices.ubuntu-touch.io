---
portType: "Halium 9.0"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "-"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+-"
      - id: "video"
        value: "+-"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
      - id: "waydroid"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "x"
---

<section id="notes">

- sound won't come up on every boot in such cases do a force reboot, it should work from next boot.
- bluetooth works except for file transfers.
- camera preview works but cant save picture and video.

More Details and status can be found [in this xda post](https://forum.xda-developers.com/t/rom-ubuntu-touch-halium-9-with-waydroid-10-support.4351735/)

</section>

<section id="manual-install">

#### Method 1: Using a flashable zip

Please download,

- [flashable zip](https://androidfilehost.com/?fid=17825722713688261051)
- [halium-boot.img](https://www.androidfilehost.com/?fid=7161016148664827562)

To install please reboot to twrp or orange fox recovery and flash halium-boot and the above zip file

#### Method 2: Using halium-install

Download and install the [Halium-install tool](https://gitlab.com/JBBgameich/halium-install/).

Download the following files:

- [halium-boot.img](https://www.androidfilehost.com/?fid=7161016148664827562)
- [system.img](https://androidfilehost.com/?fid=7161016148664801100)
- [ubports rootfs](https://ci.ubports.com/job/xenial-hybris-android9-rootfs-arm64/)

##### Unlock OEM installation

In Android activate developer mode and use MI unlock tool to do an oem unlock (skip this step if you are on a custom rom).

If this step is not done you will not be able to flash halium-boot.

##### Install TWRP or Orange fox recovery

Install the TWRP recovery as instructed in its [documentation](https://twrp.me/xiaomi/xiaomiredminote3.html).
or install Orange fox recovery from [here](https://orangefox.download/device/kenzo)

##### Reboot to recovery and format data

1. From the computer enter `adb reboot recovery`.
2. On your phone go to `wipe`->`Format Data` then enter `yes` as instructed.
3. From the PC flash `system.img` using the halium-install script `./halium-install -p ut ubuntu-touch-hybris-xenial-arm64-rootfs.tar.gz system.img`.
4. Flash `halium-boot` from pc using fastboot ` fastboot flash boot halium-boot.img`.
5. Wait, after the reboot, you should have Ubuntu Touch Running on your Kenzo :-)
6. Please reboot once more after initial setup is done.

</section>
