import portStatus from "./portStatus.json";
import deviceInfo from "./deviceInfo.json";
import portType from "./portType.json";
import releasesData from "./releases.json";

export function releaseRules() {
  return {
    type: "object",
    required: ["portType", "kernelVersion", "portStatus"],
    properties: {
      portType: {
        type: "string",
        enum: Object.keys(portType).filter((k) => k != "Unknown"),
        default:
          "<" +
          Object.keys(portType)
            .filter((k) => k != "Unknown")
            .join("|") +
          ">"
      },
      kernelVersion: {
        type: "string",
        default: "<version of the Linux kernel running in the format x.y.z>"
      },
      installLink: {
        type: "string",
        format: "url",
        default:
          "<link to install instructions (repo), if installer isn't available>"
      },
      portStatus: {
        type: "object",
        required: Object.keys(portStatus),
        properties: Object.fromEntries(
          Object.entries(portStatus).map((category) => {
            return [
              category[0],
              {
                type: "object",
                required: category[1]
                  .filter((feature) => feature.global != "x")
                  .map((feature) => feature.id),
                properties: Object.fromEntries(
                  category[1].map((feature) => [
                    feature.id,
                    {
                      type: "object",
                      required: ["value"],
                      properties: {
                        value: {
                          type: "string",
                          enum: ["+", "-", "+-", "?", "x"],
                          default: "?"
                        },
                        bugTracker: { type: "string", format: "url" }
                      }
                    }
                  ])
                ),
                additionalProperties: false,
                propertyNames: { type: "string" }
              }
            ];
          })
        ),
        additionalProperties: false,
        propertyNames: { type: "string" }
      },
      maturity: {
        type: "number",
        minimum: 0,
        maximum: 1,
        default: "<fallback value for progress calculation, MUST NOT be used>"
      }
    },
    allOf: Object.entries(portStatus)
      .map((category) =>
        category[1]
          .filter(
            (feature) =>
              feature.global == "x" &&
              feature.conditions?.some(
                (condition) => condition.global == "ignoreGlobal"
              )
          )
          .map((feature) =>
            feature.conditions.map((condition) => {
              return {
                if: {
                  properties: Object.fromEntries([
                    [condition.field, { type: "string", pattern: condition.value }]
                  ])
                },
                then: {
                  type: "object",
                  properties: {
                    portStatus: {
                      type: "object",
                      properties: Object.fromEntries([
                        [
                          category[0],
                          {
                            type: "object",
                            required: [feature.id],
                            properties: Object.fromEntries([[feature.id, {}]])
                          }
                        ]
                      ])
                    }
                  }
                }
              };
            })
          )
      )
      .flat(2)
  };
}

export function releaseDocs() {
  let rules = releaseRules();
  delete rules.allOf;
  rules.required = Object.keys(rules.properties);
  rules.properties.portStatus.default = [
    {
      categoryName: "<name of the category of the features>",
      features: [
        {
          id: "<feature ID, you can find the complete list above>",
          value: "< + | - | +- | x | ? >",
          bugTracker:
            "<link to the relevant issue report if this feature is not working>"
        }
      ]
    }
  ];
  return rules;
}

export function dataRules() {
  return {
    type: "object",
    required: [
      "name",
      "deviceType",
      "description",
      "subforum",
      "price",
      "deviceInfo",
      "contributors",
      "sources"
    ],
    properties: {
      name: { type: "string", default: "<retail name of the device>" },
      description: {
        type: "string",
        default:
          "<marketing-style description that helps a user decide if this device is right for them>"
      },
      subforum: {
        type: "string",
        default:
          "<the subforum id on forums.ubports.com, should be added in the format number/my-device>"
      },
      deviceType: {
        type: "string",
        enum: ["phone", "tablet", "tv", "other"],
        default: "<phone|tablet|tv|other>"
      },
      tag: {
        type: "string",
        enum: ["promoted", "unmaintained"],
        default: "<promoted|unmaintained>"
      },
      buyLink: {
        type: "string",
        format: "url",
        default: "<link to manufacturer's store, if it is available>"
      },
      disableBuyLink: { type: "boolean", default: "<true|false>" },
      image: {
        type: "string",
        format: "url",
        default: "<link to a picture of the device (currently unused)>"
      },
      price: {
        type: "object",
        default: {
          min: "<minimum estimated price>",
          max: "<maximum estimated price>",
          currency: "<currency in the ISO 4217 format USD|EUR>",
          currencySymbol: "<currency symbol $|€>"
        },
        properties: {
          currency: {
            type: "string",
            pattern: "^[A-Z]{3}$"
          },
          currencySymbol: {
            type: "string"
          }
        },
        oneOf: [
          {
            required: ["avg"],
            properties: {
              avg: {
                type: "integer",
                minimum: 10,
                maximum: 2000
              }
            }
          },
          {
            required: ["min", "max"],
            properties: {
              min: {
                type: "integer",
                minimum: 10,
                maximum: 2000
              },
              max: {
                type: "integer",
                minimum: 10,
                maximum: 2000
              }
            }
          }
        ]
      },
      variantOf: {
        type: "string",
        pattern: "^[a-zA-Z0-9_\\-]+$",
        not: { const: { $data: "1/codename" } },
        default: "<device codename to inherit config from>"
      },
      deviceInfo: {
        type: "object",
        required: deviceInfo.map((spec) => spec.id),
        properties: Object.fromEntries(
          deviceInfo.map((spec) => [spec.id, { type: "string" }])
        ),
        additionalProperties: false,
        propertyNames: { type: "string" }
      },
      contributors: {
        type: "array",
        minItems: 1,
        maxItems: 50,
        items: {
          type: "object",
          required: ["name", "role"],
          properties: {
            name: {
              type: "string",
              minLength: 1,
              maxLength: 40,
              default: "<contributor name>"
            },
            role: {
              type: "string",
              enum: [
                "Phone maker",
                "Maintainer",
                "Developer",
                "Tester",
                "Contributor"
              ],
              default: "<Phone maker|Maintainer|Developer|Tester|Contributor>"
            },
            renewals: {
              type: "array",
              minItems: 1,
              items: {
                type: "string",
                format: "date",
                default:
                  "<date of maintainership renewal (add a new one every 6 months)>"
              }
            },
            expired: {
              type: "boolean",
              default:
                "<mark as true if maintainership ended before the 6 months of renewal>"
            },
            forum: {
              type: "string",
              format: "url",
              default: "<link to ubports forum profile>"
            },
            photo: {
              type: "string",
              format: "url",
              default: "<link to avatar picture>"
            }
          }
        }
      },
      communityHelp: {
        type: "array",
        minItems: 1,
        maxItems: 20,
        items: {
          type: "object",
          required: ["name", "link"],
          properties: {
            name: {
              type: "string",
              minLength: 1,
              maxLength: 50,
              default: "<link label>"
            },
            link: {
              type: "string",
              format: "url",
              default:
                "<URL for the user to get help during installation and usage>"
            }
          }
        }
      },
      docLinks: {
        type: "array",
        minItems: 1,
        maxItems: 20,
        items: {
          type: "object",
          required: ["name", "link"],
          properties: {
            name: {
              type: "string",
              minLength: 1,
              maxLength: 50,
              default: "<link label>"
            },
            link: {
              type: "string",
              format: "url",
              default: "<URL to UBports docs or blog posts>"
            }
          }
        }
      },
      sources: {
        type: "object",
        required: ["portType"],
        default: {
          portType: "<reference|community|external>",
          portPath: "<path to the device group or repository>",
          deviceGroup: "<device group name>",
          deviceSource: "<device source repository name>",
          kernelSource: "<kernel source repository name>",
          issuesRepo:
            "<repository for issues tracking when it isn't the device repository>",
          showDevicePipelines:
            "<add pipelines URL to device resources true|false>",
          issuesLink:
            "<link to open an issue (for external ports or when is not the device repository)>"
        },
        properties: {
          portType: {
            type: "string",
            enum: ["reference", "community", "external"]
          },
          portPath: {
            type: "string"
          },
          deviceGroup: {
            type: "string"
          },
          deviceSource: {
            type: "string"
          },
          kernelSource: {
            type: "string"
          },
          issuesRepo: {
            type: "string"
          },
          showDevicePipelines: {
            type: "boolean"
          },
          issuesLink: {
            type: "string",
            format: "url"
          }
        },
        allOf: [
          {
            if: {
              properties: {
                portType: { enum: ["reference", "community"] }
              }
            },
            then: {
              required: [
                "portPath",
                "deviceGroup",
                "deviceSource",
                "kernelSource"
              ],
              properties: {
                portPath: {},
                deviceGroup: {},
                deviceSource: {},
                kernelSource: {}
              }
            }
          },
          {
            if: {
              properties: {
                portType: { const: "external" }
              }
            },
            then: {
              required: ["issuesLink"],
              properties: { issuesLink: {} }
            }
          }
        ]
      },
      externalLinks: {
        type: "array",
        minItems: 1,
        maxItems: 20,
        items: {
          type: "object",
          required: ["name", "link"],
          properties: {
            name: {
              type: "string",
              minLength: 1,
              maxLength: 50,
              default: "<link label>"
            },
            link: {
              type: "string",
              format: "url",
              default: "<URL for development resources>"
            },
            noActivityDetection: {
              type: "boolean",
              default: "<Disable activity checking, if it is a link to Github or Gitlab>"
            }
          }
        }
      },
      unimplementedFeatures: {
        type: "array",
        minItems: 1,
        items: {
          type: "object",
          required: ["name"],
          properties: {
            name: {
              type: "string",
              default: "<Name of the feature not supported by the OS that the device has>"
            }
          }
        }
      },
      seo: {
        type: "object",
        properties: {
          description: {
            type: "string",
            default:
              "<Page description, used as content in <meta name='description' content=''>>"
          },
          keywords: {
            type: "string",
            default:
              "<Comma-separated list of keywords, used as content in <meta name='keywords' content=''>>"
          }
        }
      }
    },
    allOf: [
      {
        if: {
          type: "object",
          required: ["sources"],
          properties: {
            sources: {
              type: "object",
              required: ["portType"],
              properties: {
                portType: { const: "external" }
              }
            }
          }
        },
        then: {
          type: "object",
          required: ["externalLinks"],
          properties: { externalLinks: {} }
        }
      }
    ]
  };
}

export function dataDocs() {
  let rules = dataRules();
  rules.required = Object.keys(rules.properties);
  rules.properties.contributors.items.required = Object.keys(
    rules.properties.contributors.items.properties
  );
  rules.properties.externalLinks.items.required = Object.keys(
    rules.properties.externalLinks.items.properties
  );
  rules.properties.seo.required = Object.keys(rules.properties.seo.properties);
  rules.properties.deviceInfo.default = [
    {
      id: "<device specification ID, you can find the complete list above>",
      value: "<device technical specification, model...>"
    }
  ];
  return rules;
}

export default function () {
  let data = dataRules();
  let release = releaseRules();

  data.type = "object";
  data.required = data.required.concat(release.required);
  Object.assign(data.properties, release.properties);
  data.allOf = (data.allOf || []).concat(release.allOf || []);

  // Validate merged data
  data.properties.release = {
    type: "string",
    enum: releasesData.list.map((release) => release.name)
  };

  return data;
}
