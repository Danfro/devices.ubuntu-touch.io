import { componentScriptLoadAll } from "@logic/client/clientNavigation.js";

export default function registerStarSelectors() {
  componentScriptLoadAll("[star-select]", (component) => {
    component.querySelectorAll("input[type='radio']").forEach((checkbox) => {
      checkbox.addEventListener("change", (e) => {
        let current = false;
        component.querySelectorAll("input[type='radio']").forEach((el) => {
          let starSpan = el.parentElement.children[1];
          if (el.checked) {
            current = true;
            component.querySelector(".star-label").innerText = el.dataset.label;
          }
          if (current) starSpan.classList.remove("text-highlight");
          else starSpan.classList.add("text-highlight");
        });
      });
    });
  });
}
