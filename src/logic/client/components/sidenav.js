import { componentScriptLoader } from "@logic/client/clientNavigation.js";
import scrollSpy from "simple-scrollspy";

export default function registerSidenav() {
  componentScriptLoader("#sidenav", () => {
    scrollSpy("#sidenav", { offset: 10 });
  });
}
