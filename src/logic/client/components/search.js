import {
  componentScriptLoader,
  onLoadAndNavigation
} from "@logic/client/clientNavigation.js";
import List from "list.js";
import progressStages from "@data/progressStages.json";

export default function registerSearch() {
  // Run once
  componentScriptLoader("#sidebarCollapse", () => {
    // Regenerate search results
    function filterDevices() {
      window.listManager.filter((device) => applyFilters(device));
      const sortOrder = Object.values(
        document.querySelectorAll("[name='sortOrder']")
      );
      const sortBy = sortOrder.filter((e) => e.checked)[0];
      window.listManager.sort(sortBy.value, {
        order: sortBy.parentElement.childNodes[1].dataset.order || "asc"
      });
    }

    // Apply search filters
    function applyFilters(device) {
      const filters = getFilterValues([
        "deviceType",
        "portType",
        "featureStage",
        "installerFilter",
        "waydroidFilter",
        "displayOutFilter",
        "release"
      ]);
      let d = device.values();
      return (
        (!filters[0].length || filters[0].includes(d.deviceType)) &&
        d.progressStage >= parseInt(filters[2]) &&
        (!filters[1].length || filters[1].includes(d.portType)) &&
        (filters[3] ? d.installerAvailable : true) &&
        (filters[4] ? d.waydroidAvailable : true) &&
        (filters[5] ? d.displayOutAvailable : true) &&
        filters[6] == d.release
      );
    }

    // Get filter values
    function getFilterValues(query) {
      return query.map((elName) => {
        let elArray = Object.values(
          document.querySelectorAll("[name='" + elName + "']")
        );
        let filterVal = elArray.filter((e) => e.checked).map((e) => e.value);
        filterVal =
          elArray[0].type == "radio"
            ? filterVal[0]
            : elArray.length == 1
              ? filterVal.length == 1
              : filterVal;
        return filterVal;
      });
    }

    function disableFilters() {
      [
        "deviceType",
        "portType",
        "installerFilter",
        "waydroidFilter",
        "displayOutFilter"
      ].forEach((elName) => {
        document
          .querySelectorAll("[name='" + elName + "']")
          .forEach((el) => (el.checked = false));
      });
      let featureStageSelector = document.querySelector(
        "[name='featureStage'][value='0']"
      );
      featureStageSelector.checked = true;
      featureStageSelector.dispatchEvent(new Event("change"));
      _paq.push([
        "trackEvent",
        "Search filters",
        document.getElementById("disableFilters").dataset.trackingname,
        window.location.pathname
      ]);
    }

    // Set search results count in UI
    function setResultsCount(count, unfilteredCount) {
      let deviceSearchCount = document.getElementById("deviceSearchCount");
      let searchNoResults = document.getElementById("searchNoResults");
      let filteredWarning = document.getElementById("filteredWarning");
      let deviceSearchMobileCount =
        document.getElementById("deviceCountMobile");

      if (unfilteredCount == 0) {
        searchNoResults.classList.remove("d-none");
        filteredWarning.classList.add("d-none");
      } else if (unfilteredCount > count) {
        searchNoResults.classList.add("d-none");
        filteredWarning.classList.remove("d-none");
      } else {
        searchNoResults.classList.add("d-none");
        filteredWarning.classList.add("d-none");
      }

      deviceSearchCount.innerText =
        count == 1
          ? "One device"
          : count == 0
            ? "No devices"
            : count + " devices";
      deviceSearchMobileCount.innerText =
        count == 1
          ? "One supported device"
          : count == 0
            ? "No supported devices"
            : count + " supported devices";
    }

    // Count number of devices when filters are disabled
    function getUnfilteredCount() {
      const releaseFilter = getFilterValues(["release"]);
      return window.listManager.items.filter(
        (d) =>
          (window.listManager.searched ? d.found : true) &&
          d.values().release == releaseFilter[0]
      ).length;
    }

    // Decorate a function to execute only once stopped typing
    function delayedCallback(cb) {
      let time;
      return () => {
        clearTimeout(time);
        time = setTimeout(() => cb(), 1000);
      };
    }

    // Filters and search field to space separated string
    function encodeKeywordAndFilters() {
      const filters = getFilterValues([
        "deviceType",
        "portType",
        "featureStage",
        "installerFilter",
        "waydroidFilter",
        "displayOutFilter",
        "release"
      ]);
      const keyword = document.querySelector("#searchInput").value;
      return [
        keyword,
        ...filters[0].map((e) => "type:" + e),
        ...filters[1].map((e) => "port:" + e),
        "compatStage:" + filters[2],
        "installer:" + filters[3],
        "waydroid:" + filters[4],
        "displayOut:" + filters[5],
        "release:" + filters[6]
      ]
        .join(" ")
        .trim();
    }

    // Initialize list.js
    window.listManager = new List("sidebarCollapse", {
      valueNames: [
        {
          data: [
            "codename",
            "release",
            "name",
            "progress",
            "price",
            "deviceType",
            "portType",
            "progressStage",
            "installerAvailable",
            "waydroidAvailable",
            "displayOutAvailable"
          ]
        }
      ],
      listClass: "devices-list",
      searchColumns: ["codename", "name"]
    });

    const mtmSearch = delayedCallback(() => {
      _paq.push([
        "trackSiteSearch",
        encodeKeywordAndFilters(),
        "Devices",
        window.listManager.matchingItems.length
      ]);
    });

    window.listManager.on("filterComplete", (e) => {
      setResultsCount(
        window.listManager.matchingItems.length,
        getUnfilteredCount()
      );
      mtmSearch();
    });
    window.listManager.on("searchComplete", (e) => {
      setResultsCount(
        window.listManager.matchingItems.length,
        getUnfilteredCount()
      );
      mtmSearch();
    });
    filterDevices();

    // Register events
    document.querySelectorAll("input[name]").forEach((el) => {
      if (el.name != "sortOrder") el.onchange = filterDevices;
    });
    document.getElementById("disableFilters").onclick = disableFilters;

    document.querySelectorAll(".toggle-sidebarCollapse").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#sidebarCollapse").classList.toggle("show");
      };
    });

    document.querySelectorAll(".advanced-filter-toggle").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#advancedFilters").classList.toggle("show");
        _paq.push([
          "trackEvent",
          "Search filters",
          el.dataset.trackingname,
          window.location.pathname
        ]);
      };
    });

    document.querySelectorAll(".advanced-filter-show").forEach((el) => {
      el.onclick = (ev) => {
        document.querySelector("#advancedFilters").classList.add("show");
        _paq.push([
          "trackEvent",
          "Search filters",
          el.dataset.trackingname,
          window.location.pathname
        ]);
      };
    });
  });

  // Always run
  onLoadAndNavigation(() => {
    // Hide sidebar on page load on mobile
    document.querySelector("#sidebarCollapse")?.classList.remove("show");
    // Mark current device
    document.querySelector(".active--exact")?.classList.remove("active--exact");
    document
      .querySelector(
        "#sidebarCollapse [href='" +
          window.location.pathname.replace(/\/+$/, "") +
          "']"
      )
      ?.classList.add("active--exact");
  });
}
