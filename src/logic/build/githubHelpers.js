class GHReqError extends Error {}

// Helper function for Github requests
export async function makeRequest(url, token = null, headers = null) {
  let response = null;
  if (token) {
    response = await fetch(url, {
      headers: {
        Authorization: "Bearer " + token,
        ...headers
      }
    });
  } else if (headers) {
    response = await fetch(url, {
      headers: headers
    });
  } else {
    response = await fetch(url);
  }
  return response;
}

// Helper function to try both authenticated and unauthenticated GH requests
export async function avoidRateLimiting(url, headers = null) {
  let response = null;
  try {
    if (!process.env.GITHUB_TOKEN)
      throw new GHReqError("GH token not provided");
    response = await makeRequest(url, process.env.GITHUB_TOKEN, headers);
    if (
      response.status == 403 &&
      Number(response.headers.get("x-ratelimit-remaining")) == 0
    )
      throw new GHReqError("Rate limit reached");
  } catch (e) {
    if (e instanceof GHReqError) {
      response = await makeRequest(url, null, headers);
      if (
        response.status == 403 &&
        Number(response.headers.get("x-ratelimit-remaining")) == 0
      )
        throw new GHReqError("Rate limit reached");
    } else throw e;
  }
  return response;
}
