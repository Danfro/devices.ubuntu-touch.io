import pMemoize from "p-memoize";
import parallelLimit from "async/parallelLimit";
import { parseFromString } from "dom-parser";

// Import data
import test, { printTests } from "./testData.js";
import elaborate from "./elaborateData.js";
import { mergeVariants, mergeReleases } from "./mergeData.js";

const getDevices = await import.meta.glob("/data/devices/*/data.md");
const getReleases = await import.meta.glob("/data/devices/*/releases/*.md");

async function getAll(memoCode) {
  // Get releases data from files
  let releases = await Promise.all(
    Object.values(getReleases).map(async (node) => {
      let release = await node();
      release.frontmatter.codename = release.file.split("/").at(-3);
      release.frontmatter.release = release.file
        .split("/")
        .pop()
        .split(".")
        .shift();
      let mdcontent = await release.compiledContent();
      release.frontmatter.content = splitMarkdown(mdcontent);
      return release.frontmatter;
    })
  );

  // Get device data from files and add basic information
  let devices = await Promise.all(
    Object.values(getDevices).map(async (node) => {
      let device = await node();
      device.frontmatter.codename = device.file.split("/").at(-2);
      let mdcontent = await device.compiledContent();
      device.frontmatter.content = splitMarkdown(mdcontent);
      device.frontmatter.filePath = device.file;
      return device.frontmatter;
    })
  );

  // Add new line before printing logs
  console.log();

  // Build variant and release overlay
  devices = mergeReleases(devices, releases);
  devices = mergeVariants(devices);

  // Get global device data from other sources
  const allDevices = await parallelLimit(
    devices.map((device) => {
      return async () => {
        const testedPage = await test(
          "test-device:" + device.codename + "@" + device.release,
          device
        );
        const elaboratedPage = await elaborate(
          "elaborate-device:" + device.codename + "@" + device.release,
          testedPage
        );
        return elaboratedPage;
      };
    }),
    20
  );

  allDevices.forEach((d) => printTests(d));
  return allDevices;
}

function splitMarkdown(mdcontent) {
  const doc = parseFromString(mdcontent);
  return Object.fromEntries(
    [
      ["credits", doc.getElementById("credits")?.innerHTML],
      ["manualInstall", doc.getElementById("manual-install")?.innerHTML],
      ["notes", doc.getElementById("notes")?.innerHTML],
      ["preinstall", doc.getElementById("preinstall")?.innerHTML]
    ].filter((e) => !!e[1])
  );
}

export default pMemoize(getAll);
