import portStatus from "@data/portStatus.json";
import deviceInfo from "@data/deviceInfo.json";
import progressStages from "@data/progressStages.json";
import ignoredCommits from "@data/ignoredCommits.json";
import portType from "@data/portType.json";
import releaseChannels from "@data/releaseChannels.json";
import releases from "@data/releases.json";
import linkDetection from "@data/linkDetection.json";
import supportStatus from "@data/supportStatus.json";

import fetchData from "./fetchData.js";
import getGitdata from "./getGitdata.js";
import getIconPaths from "./getIconPaths.js";
import * as dataUtils from "./dataUtils.js";
import * as ghUtils from "./githubHelpers.js";

import slugify from "@sindresorhus/slugify";
import { decode } from "html-entities";
import pMemoize from "p-memoize";

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

const socialIcons = getIconPaths();

async function elaborate(memoCode, device) {
  const deviceData = await fetchData("memo-device");
  const installerData = deviceData.installerData;
  const releaseChannelData = deviceData.releaseChannelData;
  const pushServerStats = deviceData.pushServerStats;
  const currencyConversions = deviceData.currencyConversions;

  // Get file history from git
  device.gitData = [].concat(
    getGitdata(device.filePath),
    getGitdata(
      process.cwd() +
        "/data/devices/" +
        (device.variantOf || device.codename) +
        ".md"
    )
  );
  device.autodetectionHints = [];
  removeIgnoredCommits(device);
  setSourceRepositories(device);
  // checkMaintained(device); // Disable maintainership renewals checks

  // Set feature default and global values
  device.globalIssuesList = [];
  if (device.portStatus) {
    addMissingFeatures(device);
    addConditionalStates(device);
    addGlobalState(device);
    listUnavailableFeatures(device);
    deleteUnavailableFeatures(device);
  } else {
    applyAllGlobalIssues(device);
  }

  // Calculate progress and stages
  calculateProgress(device);
  calculateProgressStage(device);
  countFeatures(device);

  if (device.portStatus) {
    nextStageRequirements(device);

    // Clean categories
    useNameInsteadOfFeatureId(device);
    deleteVoidCategories(device);
  }

  if (device.deviceInfo) {
    useNameInsteadOfSpecificationId(device);
  }

  // Calculate average price if min and max is known
  calculateAveragePrice(device, currencyConversions);

  // Check wired display and bluetooth for convergence
  isConvergenceEnabled(device);

  // Add port type description from port type
  addPortTypeDescription(device);

  // Elaborate fetched data
  getInstallerSupport(device, installerData);
  addProgressWeight(device);
  addGroupsToCommunityHelp(device);
  detectLinkIcons(device);
  if (pushServerStats)
    getPushStats(device, pushServerStats, installerData.alias);
  await getReleaseChannels(device, releaseChannelData, installerData.alias);
  await getRepositoryActivity(device);
  getSupportStatusIndicators(device);
  calculateSupportStatus(device);
  await downloadForumData(device);
  if (!device.noInstall)
    await getInstructionsFromInstaller(device, installerData.alias);
  getEnabledSections(device);

  // Import and set images
  await setImportedImages(device);

  return device;
}

/* Functions */

// Get installation instructions from installer configs
async function getInstructionsFromInstaller(device, installerAliases) {
  try {
    const configCodename =
      installerAliases[device.codename]?.[0] || device.codename;
    const response = await fetch(
      "https://ubports.github.io/installer-configs/v2/devices/" +
        configCodename +
        ".json"
    );
    const data = await response.json();
    let osSection = data.operating_systems.find(
      (os) => os.name == "Ubuntu Touch"
    );
    device.installerInstructions = {
      unlock: data?.unlock.map((stepId) => data?.user_actions[stepId]) || [],
      prerequisites:
        osSection?.prerequisites.map((stepId) => data?.user_actions[stepId]) ||
        [],
      downloads:
        []
          .concat(
            ...osSection?.steps
              .filter((s) =>
                s?.actions.some((a) => Object.hasOwn(a, "core:manual_download"))
              )
              .map((s) =>
                s.actions.filter((a) =>
                  Object.hasOwn(a, "core:manual_download")
                )
              )
          )
          .map((a) => a["core:manual_download"].file) || []
    };
  } catch (e) {
    console.log(
      ansiCodes.redFG + "%s" + ansiCodes.reset,
      "Install instructions download failed for: " + device.codename
    );
    device.installerInstructions = {};
  }
}

// Download forum data
async function downloadForumData(device) {
  if (device.subforum) {
    try {
      const response = await fetch(
        "https://forums.ubports.com/api/category/" + device.subforum
      );
      const data = await response.json();
      device.forumTopics = data.topics
        .filter((topic) => !topic.deleted)
        .map((topic) => {
          return {
            title: decode(topic.title),
            user: decode(topic.user.username),
            timestamp: topic.timestamp,
            views: topic.viewcount,
            link: "https://forums.ubports.com/topic/" + topic.slug,
            locked: topic.locked,
            pinned: topic.pinned
          };
        });
      device.autodetectionHints.push({
        source: "Forum category",
        status:
          "Successfully got " +
          device.codename +
          " topics from " +
          device.subforum,
        success: true,
        logs: null
      });
    } catch (e) {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Forum topic data download failed for: " + device.codename
      );
      device.forumTopics = [];
      device.autodetectionHints.push({
        source: "Forum category",
        status: "Fetching forum activity from " + device.subforum + " failed",
        success: false,
        logs: e.toString()
      });
    }
  } else {
    device.forumTopics = [];
    device.autodetectionHints.push({
      source: "Forum category",
      status: "Cannot fetch forum activity, subforum property was not provided",
      success: false,
      logs: null
    });
  }
}

// Download release channel data
async function downloadCurrentChannelData(path) {
  try {
    const response = await fetch("https://system-image.ubports.com" + path);
    const data = await response.json();
    return [
      data,
      !data.images?.length ? ["Channel " + path + " is empty"] : []
    ];
  } catch (e) {
    // Display a warning if release channel data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download release channel data."
    );
    return [
      {},
      ["Failed to download release channel data: " + path, e.toString()]
    ];
  }
}

// Add weight to installer and port data
function addProgressWeight(device) {
  device.progress = device.noInstall ? device.progress * 0.9 : device.progress;
  device.progress = device.portStatus
    ? device.progress
    : device.progress * 0.95;
  device.progress = Math.round(1000 * device.progress) / 10;
}

// Add missing features from defaults
function addMissingFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (!graphQlFeature) {
      category.features.push({
        id: feature.id,
        name: feature.name,
        value: feature.default ? feature.default : "?"
      });
    }
  });
}

// Check global state of the feature
function addGlobalState(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (
      graphQlFeature &&
      !graphQlFeature.overrideGlobal &&
      !graphQlFeature.conditionsApplied &&
      feature.global &&
      featureScale.indexOf(feature.global) <=
        featureScale.indexOf(graphQlFeature.value)
    ) {
      graphQlFeature.value = feature.global;
      graphQlFeature.bugTracker = feature.bugTracker ? feature.bugTracker : "";
      graphQlFeature.global = true;
    }

    if (
      graphQlFeature &&
      !graphQlFeature.overrideGlobal &&
      !graphQlFeature.conditionsApplied &&
      graphQlFeature.value != "x" &&
      feature.issueDescription
    ) {
      device.globalIssuesList.push({
        feature: feature.name,
        description: feature.issueDescription,
        appliesTo: feature.issueAppliesTo,
        link: feature.bugTracker,
        linkImg: feature.bugTracker ? detectLinkIcon(feature.bugTracker) : null
      });
      graphQlFeature.bugTracker = "#globalIssues";
      graphQlFeature.global = true;
    }
  });
}

// Mark state of the feature based on other conditions (mostly port type)
function addConditionalStates(device) {
  let featureScale = ["x", "-", "?", "+-", "+", "ignoreGlobal"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (feature.conditions) {
      for (let condition of feature.conditions) {
        const condValueRegex = new RegExp(condition.value);
        if (
          graphQlFeature &&
          !graphQlFeature.overrideGlobal &&
          condValueRegex.test(device[condition.field])
        ) {
          if (
            condition.global &&
            featureScale.indexOf(condition.global) <=
              featureScale.indexOf(graphQlFeature.value)
          ) {
            graphQlFeature.value = condition.global;
            graphQlFeature.bugTracker = condition.bugTracker || "";
            graphQlFeature.global = true;
          }
          if (
            graphQlFeature.value != "x" &&
            condition.issueDescription &&
            condition.global != "ignoreGlobal"
          ) {
            device.globalIssuesList.push({
              feature: feature.name,
              description: condition.issueDescription,
              appliesTo: condition.issueAppliesTo,
              link: condition.bugTracker,
              linkImg: condition.bugTracker
                ? detectLinkIcon(condition.bugTracker)
                : null
            });
            graphQlFeature.bugTracker = "#globalIssues";
            graphQlFeature.global = true;
          }
          // Ensure that global issues with conditions always have higher priority
          graphQlFeature.conditionsApplied = true;
        }
      }
    }
  });
}

// Add all the global issues if the portStatus is not specified
function applyAllGlobalIssues(device) {
  for (let portCategory in portStatus) {
    for (let feature of portStatus[portCategory]) {
      let conditionsApplied = false;
      if (feature.conditions) {
        for (let condition of feature.conditions) {
          const condValueRegex = new RegExp(condition.value);
          if (condValueRegex.test(device[condition.field])) {
            if (
              condition.issueDescription &&
              condition.global != "ignoreGlobal"
            ) {
              device.globalIssuesList.push({
                feature: feature.name,
                description: condition.issueDescription,
                appliesTo: condition.issueAppliesTo,
                link: condition.bugTracker,
                linkImg: condition.bugTracker
                  ? detectLinkIcon(condition.bugTracker)
                  : null
              });
            }
            conditionsApplied = true;
          }
        }
      }
      if (feature.issueDescription && !conditionsApplied) {
        device.globalIssuesList.push({
          feature: feature.name,
          description: feature.issueDescription,
          appliesTo: feature.issueAppliesTo,
          link: feature.bugTracker,
          linkImg: feature.bugTracker
            ? detectLinkIcon(feature.bugTracker)
            : null
        });
      }
    }
  }
}

// Delete unavailable features
function deleteUnavailableFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature && graphQlFeature.value == "x") {
      category.features.splice(category.features.indexOf(graphQlFeature), 1);
    }
  });
}

// Get installer compatibility data
function getInstallerSupport(device, installerData) {
  let configCodename =
    installerData.alias[device.codename]?.[0] || device.codename;
  let deviceInstaller = installerData.config.some(
    (el) =>
      el.codename == configCodename &&
      el.operating_systems.includes("Ubuntu Touch")
  );
  device.noInstall = !deviceInstaller;
  device.autodetectionHints.push({
    source: "Installer compatibility",
    status: deviceInstaller
      ? "Detected installer config as " + configCodename
      : "Couldn't detect installer compatibility for " + configCodename,
    success: deviceInstaller,
    logs: null
  });
}

// Match device using codename on the push server and calculate community size score
function getPushStats(device, pushServerStats, installerAliases) {
  const allCodenames = [
    device.codename,
    installerAliases[device.codename]?.[0],
    device.variantOf,
    installerAliases[device.variantOf]?.[0]
  ].filter((e) => !!e);
  const [devicePush, pushCodename] = allCodenames.reduce(
    ([result, resultCodename], codename) =>
      result
        ? [result, resultCodename]
        : [pushServerStats.devices.find((el) => el.type == codename), codename],
    [null, null]
  );

  if (devicePush) {
    let ranking = Math.ceil(Math.log(devicePush.data["5min"]) / Math.log(4));
    device.communitySize = ranking >= 1 ? ranking : 1;
    device.autodetectionHints.push({
      source: "Push server",
      status:
        "Detected community size " +
        device.communitySize +
        " with codename " +
        pushCodename,
      success: true,
      logs: null
    });
  } else {
    device.communitySize = 0;
    device.autodetectionHints.push({
      source: "Push server",
      status:
        "Codename not found on push server, tried " + allCodenames.join(", "),
      success: false,
      logs: null
    });
  }
}

// Get release channel data
async function getReleaseChannels(
  device,
  releaseChannelData,
  installerAliases
) {
  let allChannels = [];
  let logs = [];
  let configCodename =
    installerAliases[device.codename]?.[0] || device.codename;
  for (let chID in releaseChannelData) {
    if (
      !releaseChannelData[chID].hidden &&
      releaseChannelData[chID].devices[configCodename]
    ) {
      let [currentChannel, log] = await downloadCurrentChannelData(
        releaseChannelData[chID].devices[configCodename].index
      );
      logs = logs.concat(log);
      if (!currentChannel?.images?.length) continue;
      let lastOtaRelease = currentChannel.images
        .at(-1)
        .version_detail.split(",")
        .reduce((previousValue, currentValue, currentIndex, array) => {
          previousValue[currentValue.split("=").shift()] = currentValue
            .split("=")
            .pop();
          return previousValue;
        }, {});
      let channelName = chID.replace(/^(ubports\-touch\/)/, "");
      let releaseDate =
        lastOtaRelease.ubports
          ?.slice(0, 8)
          .replace(/(\d{4})(\d{2})(\d{2})/, "$1-$2-$3") ||
        lastOtaRelease.rootfs?.slice(0, 10) ||
        "1970-01-01";
      let ubuntuRelease = channelName
        .split("/")
        .filter((c) => !!c)
        .shift();
      let channelType = channelName
        .split("/")
        .filter((c) => !!c)
        .pop();
      allChannels.push({
        channel: channelType,
        fullName: chID,
        ubuntuReleaseId:
          releases.list.find((r) => r.releaseChannelsName == ubuntuRelease)
            ?.name || ubuntuRelease,
        ubuntuRelease:
          releases.list.find((r) => r.releaseChannelsName == ubuntuRelease)
            ?.prettyName || ubuntuRelease,
        version: lastOtaRelease.tag || releaseDate,
        releaseDate: new Date(releaseDate),
        description: releaseChannels[channelType].description,
        order: releaseChannels[channelType]?.order || 999
      });
    }
  }
  device.releaseChannels = allChannels
    .sort((a, b) => a.order - b.order)
    .filter((c) => c.ubuntuReleaseId == device.release);
  device.autodetectionHints.push({
    source: "Release channels",
    status: device.releaseChannels.length
      ? "Found " +
        device.releaseChannels.length +
        " release channels for " +
        configCodename
      : "Couldn't detect any release channels for " + configCodename,
    success: !!device.releaseChannels.length,
    logs: !!logs.length ? logs.join("\n") : null
  });
}

// Calculate porting progress from feature matrix ( use maturity field as fallback )
function calculateProgress(device) {
  if (device.portStatus) {
    let totalWeight = 0,
      currentWeight = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        totalWeight += feature.weight;
        currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;
      }
    });

    device.progress = currentWeight / totalWeight;
  } else {
    // Fallback from maturity
    device.progress = device.maturity;
  }
}

// Count the number of features marked as the provided value
function countFeatureMarkedAs(device, value) {
  let count = 0;
  if (device.portStatus) {
    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        count += graphQlFeature.value == value ? 1 : 0;
      }
    });
  }
  return count;
}

// Count number of features by value
function countFeatures(device) {
  device.featureCount = Object.fromEntries(
    [
      { id: "untested", val: "?" },
      { id: "working", val: "+" },
      { id: "partial", val: "+-" },
      { id: "notWorking", val: "-" }
    ].map((test) => {
      return [test.id, countFeatureMarkedAs(device, test.val)];
    })
  );
}

// List unavailable features
function listUnavailableFeatures(device) {
  let unavailableFeatures = [];
  if (device.portStatus) {
    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (
        graphQlFeature &&
        graphQlFeature.value == "x" &&
        !graphQlFeature.global &&
        !feature.ignoreUnavailable
      ) {
        unavailableFeatures.push({
          name: feature.name
        });
      }
    });
  }
  device.unavailableFeatures = unavailableFeatures;
}

// Calculate progress stage
function calculateProgressStage(device) {
  let currentStageIndex = 0; // Fallback from maturity
  if (device.portStatus) {
    currentStageIndex = progressStages.length - 1; // Daily-driver ready

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature && graphQlFeature.value != "+") {
        if (currentStageIndex >= feature.stage && feature.stage > 0) {
          currentStageIndex = feature.stage - 1;
        }
      }
    });
  }

  device.progressStage = {
    number: currentStageIndex,
    name: progressStages[currentStageIndex].name,
    description: progressStages[currentStageIndex].description
  };
}

// Fetch last activity for repositories listed in sources
async function getRepositoryActivity(device) {
  let repos = [
    {
      service: "gitlab",
      id: getRepositoryUrl(device, "device", "id")
    },
    {
      service: "gitlab",
      id: getRepositoryUrl(device, "kernel", "id")
    }
  ].filter((r) => !!r.id);
  let lastActivity = null;
  let lastActivityRepo = null;
  let logs = [];

  if (repos.length == 0 && device.sources?.portType == "external") {
    device.externalLinks?.forEach((link) => {
      if (!link.noActivityDetection) {
        if (link.link.startsWith("https://github.com/")) {
          repos.push({
            service: "github",
            id: link.link
              .match(/(?<=https:\/\/github\.com\/)\S+$/)[0]
              .replace(/\/+$/, "")
          });
        } else if (link.link.startsWith("https://gitlab.com/")) {
          repos.push({
            service: "gitlab",
            id: link.link
              .match(/(?<=https:\/\/gitlab\.com\/)\S+$/)[0]
              .replace(/\/+$/, "")
          });
        }
      }
    });
  }

  if (repos.length == 0) {
    console.log(
      "[" +
        device.codename +
        "@" +
        device.release +
        "] " +
        "has no sources available, not checking activity"
    );
    logs.push("Sources not found in device.sources or device.externalLinks");
  }

  for (let r of repos) {
    let repoLastDate = null;
    if (r.service == "gitlab") {
      const response = await fetch(
        "https://gitlab.com/api/v4/projects/" +
          encodeURIComponent(r.id) +
          "/repository/branches"
      );
      const data = await response.json();
      if (!data.message)
        repoLastDate = data
          .map((branch) => new Date(branch.commit.committed_date))
          .sort((a, b) => b - a)[0];
      else {
        console.log("Failed to get GL: " + r.id);
        logs.push("Failed to get " + r.id + " from Gitlab API");
        logs.push(data.message);
      }
    } else if (r.service == "github" && !!process.env.GITHUB_TOKEN) {
      const branchesRequest = await ghUtils.avoidRateLimiting(
        "https://api.github.com/repos/" + r.id + "/branches"
      );
      const branches = await branchesRequest.json();
      if (!branches.message) {
        let allDates = [];
        for (let branch of branches) {
          const response = await ghUtils.avoidRateLimiting(
            "https://api.github.com/repos/" +
              r.id +
              "/commits?sha=" +
              encodeURIComponent(branch.name),
            { "If-Modified-Since": new Date().toGMTString() }
          );
          let dateFromHeader = response.headers.get("last-modified");
          if (!!dateFromHeader) allDates.push(new Date(dateFromHeader));
        }
        repoLastDate =
          allDates.length > 0 ? allDates.sort((a, b) => b - a)[0] : null;
      }
      if (repoLastDate === null) {
        console.log("Failed to get GH: " + r.id);
        logs.push("Failed to get " + r.id + " from Github API");
        if (branches.message) logs.push(branches.message);
      }
    } else if (r.service == "github" && !process.env.GITHUB_TOKEN) {
      logs.push(
        "Fetching from Github disabled: missing token, " + r.id + " skipped"
      );
    }
    if (repoLastDate && (lastActivity === null || repoLastDate > lastActivity))
      [lastActivity, lastActivityRepo] = [repoLastDate, r];
  }
  device.sources = device.sources || {};
  device.sources.lastActivity = lastActivity;
  device.autodetectionHints.push({
    source: "Repository activity",
    status: !!lastActivity
      ? "Last activity from " +
        lastActivityRepo.id +
        " at " +
        lastActivityRepo.service
      : "Couldn't fetch repositories",
    success: !!lastActivity,
    logs: !!logs.length ? logs.join("\n") : null
  });
}

// Get support status indicators
function getSupportStatusIndicators(device) {
  let phoneMakerSupport = !!device.contributors?.filter(
    (c) => c.role == "Phone maker" && !c.expired
  ).length;
  let nMonthsAgo = new Date();
  nMonthsAgo.setMonth(nMonthsAgo.getMonth() - 6);

  device.supportIndicators = {
    minimalFunctions: device.progressStage?.number >= 2,
    essentialFunctions: device.progressStage?.number >= 4,
    fullFunctions: device.progressStage?.number >= 5,
    optionalFunctions:
      device.progressStage?.number >= 5 &&
      device.featureCount.untested +
        device.featureCount.notWorking +
        device.featureCount.partial ==
        0,
    installer: !device.noInstall,
    maintainership:
      (phoneMakerSupport ||
        !!device.contributors?.filter((c) => c.role == "Maintainer").length) &&
      device.tag != "unmaintained", // If the requirement is too strict change the && to ||
    phoneMakerSupport: phoneMakerSupport,
    activeness:
      device.sources?.lastActivity !== null
        ? device.sources?.lastActivity > nMonthsAgo
        : null,
    stableReleaseChannel: device.releaseChannels.some(
      (c) => c.channel == "stable"
    ),
    releaseChannels: !!device.releaseChannels.length,
    sourcesProvided: !!device.sources?.portType,
    ubportsHostedRepository:
      device.sources?.portType == "community" ||
      device.sources?.portType == "reference",
    upToDateRelase: !releases.list.find((r) => r.name == device.release)
      .supersededBy
  };
}

// Calculate support status from data
function calculateSupportStatus(device) {
  let supportIndex = supportStatus.findLastIndex((stage) =>
    stage.requirements.every((req) => device.supportIndicators[req])
  );

  device.supportStatus = {
    number: supportIndex,
    name: supportStatus[supportIndex].name,
    description: supportStatus[supportIndex].description
  };
}

// Get next stage and required features to reach it
function nextStageRequirements(device) {
  let missing = [];
  let nextStage = device.progressStage.number + 1;

  if (nextStage == progressStages.length) return;

  dataUtils.forEachFeature(device, function (feature, category) {
    if (feature.stage == nextStage) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);
      if (graphQlFeature && graphQlFeature.value != "+") {
        missing.push(category.categoryName + ": " + feature.name);
      }
    }
  });

  device.nextProgressStage = {
    number: nextStage,
    name: progressStages[nextStage].name,
    description: progressStages[nextStage].description
  };
  device.nextStageRequirements = missing;
}

// Add wired convergence availability
function isConvergenceEnabled(device) {
  try {
    let wiredExternalMonitor = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "USB"),
      "wiredExternalMonitor"
    );

    let bluetooth = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "Network"),
      "bluetooth"
    );

    device.isConvergenceEnabled =
      (bluetooth.value == "+" || bluetooth.value == "+-") &&
      (wiredExternalMonitor.value == "+" || wiredExternalMonitor.value == "+-");
  } catch (e) {
    device.isConvergenceEnabled = false;
  }
}

// Add port type description
function addPortTypeDescription(device) {
  if (device.portType) {
    device.portTypeDescription = portType[device.portType]
      ? portType[device.portType]
      : portType["Unknown"];
  } else {
    device.portTypeDescription = portType["Unknown"];
  }
}

// Get list of enabled sections
function getEnabledSections(device) {
  device.enabledSections = {
    portStatus: !!device.portStatus,
    deviceSpec: !!device.deviceInfo,
    helpWanted:
      device.featureCount.untested > 0 ||
      (!device.noInstall && device.progress > 95),
    docLinks: !!device.docLinks,
    portContributors: !!device.contributors,
    installerDownload: !device.noInstall,
    discussionsbar:
      !!device.forumTopics || !!device.communityHelp || !!device.notValid
  };
}

// Calculate average price
async function calculateAveragePrice(device, currencyConversions) {
  if (device.price) {
    if (!device.price?.avg && device.price?.min && device.price?.max)
      device.price.avg = (device.price.min + device.price.max) / 2;
    device.price.sortableAvg =
      device.price?.currency && device.price?.currency != "USD"
        ? (
            device.price?.avg / currencyConversions[device.price.currency]
          ).toFixed()
        : device.price?.avg;
  }
}

// Add UBports groups and subforum to communityHelp
function addGroupsToCommunityHelp(device) {
  let links = [
    {
      name: "Device subforum",
      link: device.subforum
        ? "https://forums.ubports.com/category/" + device.subforum
        : null
    },
    {
      name: "Telegram - installation help",
      link: !device.noInstall ? "https://t.me/WelcomePlus" : null
    },
    {
      name: "Telegram - @ubports",
      link: "https://t.me/ubports"
    }
  ].filter((l) => !!l.link);

  device.communityHelp = [...links, ...(device.communityHelp || [])];
}

// Get the url of the repository from the sources field
function getRepositoryUrl(device, repo, reqType = "url") {
  if (!device.sources) return null;
  if (device.sources.portType == "external") return null;

  let urls = {};

  let portingGroup =
    reqType == "url"
      ? "https://gitlab.com/ubports/porting"
      : reqType == "id"
        ? "ubports/porting"
        : null;
  if (portingGroup == null) return null;

  urls.base =
    device.sources.portType == "reference"
      ? portingGroup + "/reference-device-ports"
      : device.sources.portType == "community"
        ? portingGroup + "/community-ports"
        : null;

  urls.group = urls.base
    ? [urls.base, device.sources.portPath, device.sources.deviceGroup]
        .filter((f) => !!f)
        .join("/")
    : null;

  urls.issues =
    device.sources.issuesLink ??
    (urls.group
      ? device.sources.issuesRepo
        ? urls.group + "/" + device.sources.issuesRepo + "/-/issues"
        : device.sources.deviceSource
          ? urls.group + "/" + device.sources.deviceSource + "/-/issues"
          : device.sources.deviceGroup
            ? urls.group + "/-/issues"
            : null
      : null);

  urls.device =
    urls.group && device.sources.deviceSource
      ? urls.group + "/" + device.sources.deviceSource
      : null;

  urls.kernel =
    urls.group && device.sources.kernelSource
      ? urls.group + "/" + device.sources.kernelSource
      : null;

  urls.ci =
    urls.group && urls.device && device.sources.showDevicePipelines
      ? urls.device + "/-/pipelines"
      : null;

  return urls[repo];
}

// Add source links to externalLinks
function setSourceRepositories(device) {
  if (device.sources) {
    let baseUrl = getRepositoryUrl(device, "base");

    let groupUrl = getRepositoryUrl(device, "group");

    device.sources.issuesLink = getRepositoryUrl(device, "issues");

    let links = [
      {
        name: "Device repositories",
        link:
          groupUrl &&
          device.sources.deviceGroup &&
          !device.sources.deviceSource &&
          !device.sources.kernelSource
            ? groupUrl
            : null
      },
      {
        name: "Device source",
        link: getRepositoryUrl(device, "device")
      },
      {
        name: "Kernel source",
        link: getRepositoryUrl(device, "kernel")
      },
      {
        name: "CI builds",
        link: getRepositoryUrl(device, "ci")
      },
      {
        name: "Report an issue",
        link: device.sources.issuesLink || null
      }
    ].filter((l) => !!l.link);

    device.externalLinks = [...links, ...(device.externalLinks || [])];
  }
}

function detectLinkIcon(link) {
  let detectedLink = "rss";
  for (let linkRegex of linkDetection) {
    let pattern = new RegExp(linkRegex.regex);
    if (!!pattern.test(link)) {
      detectedLink = linkRegex.icon;
      break;
    }
  }
  return detectedLink;
}

// Detect link icons
function detectLinkIcons(device) {
  let detectLink = (link) => {
    link.icon = detectLinkIcon(link.link);
  };

  device.externalLinks?.forEach(detectLink);
  device.communityHelp?.forEach(detectLink);
}

// Check maintainership status
function checkMaintained(device) {
  // Find who is maintainer
  device.contributors
    ?.filter((c) => c.role == "Maintainer")
    .forEach((maintainer) => {
      if (!maintainer.renewals?.length) {
        maintainer.role = "Developer";
        return;
      }
      let expiryDate = new Date(maintainer.renewals.at(-1));
      expiryDate.setMonth(expiryDate.getMonth() + 6);
      maintainer.role =
        !maintainer.expired && expiryDate.getTime() > new Date().getTime()
          ? "Maintainer"
          : "Developer";
    });
}

// Replace ID with name
function useNameInsteadOfFeatureId(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature) {
      graphQlFeature.name = feature.name;
    }
  });
}

// Delete categories that have no features set
function deleteVoidCategories(device) {
  for (let portCategory in portStatus) {
    let category = dataUtils.getCategoryByName(device, portCategory);

    if (category && category.features.length == 0) {
      device.portStatus.splice(device.portStatus.indexOf(category), 1);
    }
  }
}

// Elaborate device info from IDs
function useNameInsteadOfSpecificationId(device) {
  device.deviceInfo.forEach((el) => {
    el.name = deviceInfo.find((info) => info.id == el.id).name;
  });
}

// Remove development commits from the data history
function removeIgnoredCommits(device) {
  for (let commit = device.gitData.length - 2; commit >= 0; commit--) {
    if (ignoredCommits.includes(device.gitData[commit].hash)) {
      device.gitData.splice(commit, 1);
    }
  }
}

// Set imported images
async function setImportedImages(device) {
  if (device.externalLinks) {
    let extLinks = [];

    for (let link of device.externalLinks) {
      extLinks.push({
        name: link.name,
        link: link.link,
        icon: socialIcons[link.icon]
      });
    }
    device.externalLinks = extLinks;
  }

  if (device.communityHelp) {
    let extLinks = [];

    for (let link of device.communityHelp) {
      extLinks.push({
        name: link.name,
        link: link.link,
        icon: socialIcons[link.icon]
      });
    }
    device.communityHelp = extLinks;
  }

  if (device.globalIssuesList) {
    let extLinks = [];

    for (let link of device.globalIssuesList) {
      let linkIcon = link.linkImg;
      if (!!linkIcon) link.linkImg = socialIcons[linkIcon];
    }
  }
}

export default pMemoize(elaborate);
