---
// Import libraries
import { empty } from "empty-schema";
import YAML from "js-yaml";
import { dataDocs, releaseDocs } from "@data/validationRules.js";
import { deviceInfoAsArray } from "@logic/build/dataUtils.js";

// Import layouts
import Layout from "@layouts/Page.astro";

// Import data
import portStatus from "@data/portStatus.json";
import deviceInfo from "@data/deviceInfo.json";
const dataSchema = dataDocs();
const releaseSchema = releaseDocs();

const pageMeta = {
  title: "Device file specifications • Ubuntu Touch • Linux Phone",
  meta: [
    {
      name: "keywords",
      content:
        "Ubuntu Touch, mobile, Linux, Android, privacy, smartphone, developer"
    },
    {
      name: "description",
      content:
        "Explore devices compatible with Ubuntu Touch. Documentation about adding and updating devices shown on the Ubuntu Touch devices website."
    },
    {
      property: "og:url",
      content: "https://devices.ubuntu-touch.io"
    },
    {
      property: "og:title",
      content: "Device file specifications • Ubuntu Touch • Linux Phone"
    },
    {
      property: "og:description",
      content:
        "Explore devices compatible with Ubuntu Touch. Documentation about adding and updating devices shown on the Ubuntu Touch devices website."
    }
  ]
};
---

<Layout pageMeta={pageMeta}>
  <!-- Content -->
  <div></div>
  <div class="wide-content markdown-content wrap-wide-content">
    <h1 id="device-data-format-specification" class="mt-4">
      Device data format specification
    </h1>
    <p class="mt-2">
      The devices shown on this website are added from markdown and yaml files
      stored at <a
        href="https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/"
        target="_blank">the website repository</a
      > in the <a
        href="https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices"
        target="_blank">data/devices</a
      > folder. Each device has its own folder named by the codename the manufacturer
      has assigned to the device.
    </p>
    <p class="mt-2">
      The device folder has a main file called <code>data.md</code> that contains
      all the data that does not change between software releases. Data on the device
      type and specifications will be placed in this file.
    </p>
    <p class="mt-2">
      In the same folder is located the <code>releases</code> directory that contains
      a markdown file for each version of Ubuntu Touch available. These files are
      named after the adjective of the Ubuntu release on which the release is based,
      for example <code>xenial.md</code> or <code>focal.md</code>.
    </p>
    <p class="mt-2">We will then have a structure similar to the following:</p>
    <pre><code>suzu
├── data.md
└── releases
    ├── focal.md
    └── xenial.md</code></pre>
    <h2 id="adding-new-devices">Adding new devices</h2>
    <p class="mt-2">
      You can add new devices by creating a Markdown file with a YAML metadata
      block under <code>data/devices/&lt;codename&gt;/data.md</code> and a Markdown
      file for each release available under <code
        >data/devices/&lt;codename&gt;/releases/&lt;release&gt;.md</code
      >. The YAML metadata block is a snippet of YAML in your otherwise
      Markdown-formatted document, and is denoted by three dashes alone on the
      line above and below the block.
    </p>
    <pre><code>---
name: My Ported Device
deviceType: phone
...
---

&lt;section id="notes"&gt;

## You should know...

This device occasionally requests an offering of bacon. Don&#39;t ask, just provide.

&lt;/section&gt;

&lt;section id="preinstall"&gt;

## Preinstallation instructions

Before using the UBports Installer on this device, ...

&lt;/section&gt;</code></pre>
    <p class="mt-2">
      There are complete templates of device markdown files for <a
        href="/api/templates/data.md"
        data-ignore-navigation
        download
        class="matomo_download">devices</a
      > and <a
        href="/api/templates/release.md"
        data-ignore-navigation
        download
        class="matomo_download">releases</a
      > downloadable from the website. Copy that document and fill in your device’s
      information as needed. Note that some optional sections which are listed in
      the <a href="#yaml-metadata-block-reference"
        >YAML metadata block reference</a
      > are not included in the example document. Carefully review the available
      information and fill in as much as you can.
    </p>
    <h3 id="device-file">Device file</h3>
    <p class="mt-2">
      Your YAML metadata block will have four primary sections. For more
      information on filling these sections, see the <a
        href="#yaml-metadata-block-reference">YAML metadata block reference</a
      > below.
    </p>
    <h4 id="device-description">Device description</h4>
    <pre><code>name: &quot;My Cool Device&quot;
deviceType: &quot;phone&quot;
description: &quot;My Cool Device features an absurd 100:1 screen ratio that ensures you never have to scroll a webpage.&quot;
buyLink: &quot;https://example.com&quot;</code></pre>
    <p class="mt-2">
      The root level of the YAML contains base information about the device,
      such as its name and type.
    </p>
    <h4 id="the-subforum">The subforum</h4>
    <pre><code>subforum: &lt;the subforum id on forums.ubports.com, should be added in the format number/my-device&gt;</code></pre>
    <p class="mt-2">
      The subforum is a category in the UBports forum to discuss your device
      port with users and developers. If you would like a forum category for
      your device, make a post in the forum’s General section.
    </p>
    <p class="mt-2">
      The subforum id is necessary to display forum posts in the sidebar. To add
      it search for the subforum on <a
        href="https://forums.ubports.com"
        target="_blank">the Ubports forum</a
      >. The url should look like this
      “https://forums.ubports.com/category/number/my-awesome-device”. Take only
      the last two parameters in the url bar that should be in the format
      “number/my-awesome-device”.
    </p>
    <h4 id="device-variants">Device variants</h4>
    <pre><code>variantOf: &quot;main-device-codename&quot;</code></pre>
    <p class="mt-2">
      In case your device has many codenames or you are grouping devices of the
      same port you can use the <code>variantOf</code> keyword to inherit properties
      of another device by its codename.
    </p>
    <h4 id="device-specifications">Device specifications</h4>
    <pre><code>deviceInfo:
  - id: &quot;chipset&quot;
    value: &quot;Qualcomm MSM8956 Snapdragon 650&quot;</code></pre>
    <p class="mt-2">
      The deviceInfo section contains an overview of the device’s
      specifications. The following data should be filled in:
    </p>
    <div class="table-responsive mt-3 mb-3">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Specification ID</th>
            <th>Specification name</th>
          </tr>
        </thead>
        <tbody>
          {
            deviceInfo.map((spec) => (
              <tr>
                <td>{spec.id}</td>
                <td>{spec.name}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
    <h4 id="contributors">Contributors</h4>
    <pre><code>contributors:
  - name: Yumi
    photo: &lt;link to avatar picture&gt;
    forum: &lt;link to ubports forum profile&gt;
    role: &lt;Phone maker|Maintainer|Developer|Tester|Contributor&gt;
    renewals:
        - &lt;Maintainership renewal date in format YYYY-MM-DD (maintainers only)&gt;</code></pre>
    <p class="mt-2">
      This is a place to credit the people who have worked on this device port.
      List them by name and their UBports Forum profile link. If they have a
      profile picture set on the forum, copy the image link for that picture and
      add it as the <code>photo</code> key. Otherwise, a 3D render of the Yumi mascot
      for UBports will be used instead.
    </p>
    <p class="mt-2">
      The devices website also keeps track of device maintenance. Devices
      running Ubuntu Touch need to be updated from time to time on a
      device-specific basis in order to enable new features or complete porting
      progress. In addition, during system updates following the completion of
      porting, it is possible that some changes may cause a device's
      functionality to regress. Therefore, and in order to apply security
      patches at the hardware level, we need someone who is willing to perform
      these functions for an extended period of time.
    </p>
    <p class="mt-2">
      If you would like to become a maintainer of your device, you can register
      by setting the role to maintainer and writing today's date for the first
      renewal. Each renewal lasts for 6 months, before the end of which you can
      renew the maintainer role by writing a new date in the renewals in a merge
      request. In case you can no longer fulfil the role of maintainer you can
      always open a merge request to inform us and set the property <code
        >expired: true</code
      >.
    </p>
    <p class="mt-2">
      If the phone manufacturer supports Ubuntu Touch on the device, the "Phone
      maker" role can be added, which for the purposes of maintaining the device
      is equivalent to the maintainer but does not require periodic renewals.
      When the manufacturer decides to stop providing updates, they can inform
      us by setting the <code>expired: true</code> property.
    </p>
    <h4 id="sources-and-bug-tracking">Sources and bug tracking</h4>
    <pre><code>sources:
  portType: &quot;community&quot;
  portPath: &quot;android9&quot;
  deviceGroup: &quot;volla-phone&quot;
  deviceSource: &quot;volla-yggdrasilx&quot;
  kernelSource: &quot;kernel-volla-mt6763&quot;
  issuesLink: &quot;https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues&quot;</code></pre>
    <p class="mt-2">
      In order to list your device on the website, you must publish the sources.
      Devices will be grouped into three categories based on where the sources
      are published:
    </p>
    <ul>
      <li>
        Reference devices: devices that the core developers have and are
        developing and testing Ubuntu Touch on are <a
          href="https://gitlab.com/ubports/porting/reference-device-ports/"
          target="_blank">listed here</a
        >
      </li>
      <li>
        Community devices: devices developed by the community and accepted in
        the <a
          href="https://gitlab.com/ubports/porting/community-ports/"
          target="_blank">Ubports' Gitlab repositories</a
        >
      </li>
      <li>
        External devices: devices with sources hosted on public repositories
        owned by the developer
      </li>
    </ul>
    <p class="mt-2">
      When listing a reference device or a community device you will need to set
      the portPath to the device group starting from the reference or community
      ports group on Gitlab and the id of the group in the deviceGroup property.
      Then set the deviceSource and kernelSource links to the correct IDs of the
      repositories in the device group. The issuesLink is not required for
      reference and community ports, it is required only for external ports, it
      needs to be set to an URL where people can report problems they’re having
      with your port. On reference and community devices, the issuesLink is the
      issues URL of the device source repository, if it needs to be changed it
      can be overwritten with the issuesRepo or issuesLink property.
    </p>
    <h4 id="external-links-community-help-and-doc-links">
      External links, community help and doc links
    </h4>
    <pre><code>externalLinks:
  - name: &quot;Issue tracker&quot;
    link: &quot;https://gitlab.com/ubports/ubuntu-touch/-/issues&quot;</code></pre>
    <p class="mt-2">
      External links help visitors find more information about your device port.
      This section will be added automatically if the sources of your device are
      available in the community or reference port repositories. If your device
      is located in an external repository you will need to add manually the
      links to the sources in this section. Reference and community ports can
      also use this section to add more details and useful links that are not
      already listed in the sources section. The link for issue reports is
      automatically added for all the ports from the sources and it shouldn't be
      added to the links.
    </p>
    <p class="mt-2">
      For ports with external sources all relevant source repositories (device,
      device_common, kernel, manifest) should be linked if possible.
    </p>
    <pre><code>communityHelp:
  - name: &quot;Telegram - @utonvolla&quot;
    link: &quot;https://t.me/utonvolla&quot;</code></pre>
    <p class="mt-2">
      Community help links help visitors to get help with using the device. This
      is the place to add device specific groups to help users and developers
      with installation and testing. DO NOT add the device subforum, the main
      UBports group or the WelcomePlus group, they will be added automatically.
    </p>
    <p class="mt-2">
      Doc links are links to UBports docs and blog posts that mention the device
      or might be useful to the user.
    </p>
    <h3 id="releases-file">Releases file</h3>
    <p class="mt-2">
      You’ll need to add a release file to display a working release on the
      devices website. This file contains the feature matrix and information
      about the porting type.
    </p>
    <h4 id="feature-ids">Feature IDs</h4>
    <pre><code>portStatus:
  - categoryName: &quot;Sound&quot;
    - id: &quot;earphones&quot;
      value: &quot;-&quot;
      bugTracker: &quot;https://link to my bug report&quot;</code></pre>
    <p class="mt-2">
      The portStatus matrix allows the site to display the hardware
      compatibility of your port. This helps users discover ports that meet
      their needs and sets expectations for people looking to buy a device for
      Ubuntu Touch.
    </p>
    <p class="mt-2">
      If your device has hardware support for a given feature and it can be used
      under Ubuntu Touch, set the <code>value:</code> key to <code>"+"</code>.
      This marks the feature as working.
    </p>
    <p class="mt-2">
      If your device does not have hardware support for a given feature, set the

      <code>value:</code> key to <code>"x"</code>. The site will ignore any
      missing features when calculating the progress value for your device.
    </p>
    <p class="mt-2">
      If your device has hardware support for a feature but it does not work
      when running Ubuntu Touch, set the <code>value:</code> key to <code
        >"-"</code
      >. This marks the feature as broken.
    </p>
    <p class="mt-2">
      Mark a feature as partially working if your device has hardware support
      for the feature <em>and</em> it works most of the time when running Ubuntu
      Touch, <em>but</em> most users will encounter a critical issue with the feature
      within 1 week of booting the device. The <code>value:</code> key for partially
      working features is <code>"+-"</code>
    </p>
    <p class="mt-2">
      If you haven’t tested a feature or you don’t know its state you can set
      the <code>value:</code> key to <code>"?"</code>.
    </p>
    <p class="mt-2">
      If possible, include a bug report link with the <code>bugTracker</code>
      key when marking a feature as broken or partially working.
    </p>
    <div class="table-responsive mt-3 mb-3">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Category</th>
            <th>Feature ID</th>
            <th>Feature name</th>
          </tr>
        </thead>
        <tbody>
          {
            Object.entries(portStatus).map((category) => (
              <>
                {category[1].map((feature, i) => (
                  <tr>
                    {i == 0 && (
                      <td rowspan={category[1].length}>{category[0]}</td>
                    )}
                    <td>{category[1][i].id}</td>
                    <td>{category[1][i].name}</td>
                  </tr>
                ))}
              </>
            ))
          }
        </tbody>
      </table>
    </div>
    <h4 id="the-markdown-block">The markdown block</h4>
    <pre><code>---

&lt;section id="preinstall"&gt;

- This is the Markdown list
- Before using the UBports Installer on this device, ...

&lt;/section&gt;

&lt;section id="notes"&gt;

## This is the Markdown block

I can write Markdown content here. Have [a link](https://example.com)!

&lt;/section&gt;</code></pre>
    <p class="mt-2">
      The markdown block of your file will be displayed in different places on
      the page of your device. Four sections can be added.
    </p>
    <p class="mt-2">
      The <code>preinstall</code> section should contain instructions for steps required
      before using the UBports installer to flash the device. For example, if your
      device needs to have a certain version of Android installed before running
      the UBports Installer. This section overrides the instructions fetched from
      the installer configuration file, if any.
    </p>
    <p class="mt-2">
      The <code>manual-install</code> section provides installation instructions
      if the installer for the port is not yet available. In this case, it is recommended
      that the port maintainer host the installation instructions themselves, and
      link to them in the installLink property. Manual installation is not supported
      by the community. This section is maintained for backwards compatibility.
    </p>
    <p class="mt-2">
      The <code>credits</code> section allows you to add links to indirect contributors
      and projects from which your work is derived.
    </p>
    <p class="mt-2">
      The <code>notes</code> section allows the porter to add additional content
      to the page, such as port status and instructions for using the device.
    </p>
    <p class="mt-2">
      Do not place a description or a marketing blurb in the markdown block. It
      will look out of place. Use the <code>description</code> key in the YAML metadata
      block instead.
    </p>
    <h2 id="yaml-metadata-block-reference">YAML metadata block reference</h2>
    <p class="mt-2">
      The YAML metadata block for <code>data.md</code> should have the following
      format:
    </p>
    <pre><code>{YAML.dump(empty(dataSchema), { forceQuotes: true, quotingType: '"'})}</code></pre>
    <p class="mt-2">
      The YAML metadata block for <code>release.md</code> should have the following
      format:
    </p>
    <pre><code>{YAML.dump(empty(releaseSchema), { forceQuotes: true, quotingType: '"'})}</code></pre>
  </div>
</Layout>
